<?php
/* ------------------------------------- */
/* ENQUEUE JAVASCRIPTS + CSS */
/* ------------------------------------- */

function loadJSCSS() {

	$absolute_path = __FILE__;
	$path_to_file = explode( 'wp-content', $absolute_path );
	$path_to_wp = $path_to_file[0];
	require_once( $path_to_wp.'/wp-load.php' );

	// Themeoptions
	$themeoptions = getThemeOptions();

	/* Google Font */
	$headlinefonturl = $themeoptions["themetastic_font_headlineurl"];
	
	if ( ! function_exists( 'is_ssl' ) ) {
	  function is_ssl() {
	   if ( isset($_SERVER['HTTPS']) ) {
	    if ( 'on' == strtolower($_SERVER['HTTPS']) )
	     return true;
	    if ( '1' == $_SERVER['HTTPS'] )
	     return true;
	   } elseif ( isset($_SERVER['SERVER_PORT']) && ( '443' == $_SERVER['SERVER_PORT'] ) ) {
	    return true;
	   }
	   return false;
	  }
	 }
	
	 if ( version_compare( get_bloginfo( 'version' ) , '3.0' , '<' ) && is_ssl() ) {
	  $wp_content_url = str_replace( 'http://' , 'https://' , get_option( 'siteurl' ) );
	 } else {
	  $wp_content_url = get_option( 'siteurl' );
	 }
	 
	 $wp_content_url .= '/wp-content';
	 $wp_content_dir = ABSPATH . 'wp-content';
	 $wp_plugin_url = $wp_content_url . '/plugins';
	
	if (!is_admin()) {
		wp_enqueue_script( 'jquery' );

		//Google Font
		if(!empty($headlinefonturl)) wp_enqueue_style( 'themetastic_googlefont_style',$headlinefonturl);

		//Fontello Icons
	    wp_enqueue_style( 'themetastic_fontello_style',themetastic_TYPE.'/fontello.css');

	    //FancyBox2 Helpers
	    wp_enqueue_style( 'themetastic_fancybox_style',themetastic_JAVASCRIPT.'/fancybox/jquery.fancybox.css?v=2.1.3');

		//Slider Style
		wp_enqueue_style( 'themetastic_slider_style',themetastic_CSS.'/slider.css');
		
		// Enqueue the Theme Styles
		wp_enqueue_style( 'themetastic_bootstrap_style',themetastic_CSS.'/bootstrap.min.css');
		if (isset($themeoptions["themetastic_responsive"])){
			wp_enqueue_style( 'themetastic_bootstrap-responsive_style',themetastic_CSS.'/bootstrap-responsive.min.css');
		}

		// Main CSS
	    wp_enqueue_style( 'themetastic_wp_style',get_stylesheet_directory_uri().'/style.css');
	    
	    // Visual Composer Fix
		wp_enqueue_style( 'visual_composer_font_css',$wp_plugin_url.'/js_composer/assets/css/js_composer_front.css?ver=3.6.12');
		//wp_enqueue_style( 'visual_composer_custom_css',$wp_plugin_url.'/js_composer/custom.css?ver=3.6.12');
		wp_register_style( 'flexslider', $wp_plugin_url.'/js_composer/assets/lib/flexslider/flexslider.css' , false, null, 'screen' );
        wp_register_style( 'nivo-slider-css', $wp_plugin_url.'/js_composer/assets/lib/nivoslider/nivo-slider.css' , false, null, 'screen' );
        wp_register_style( 'nivo-slider-theme', $wp_plugin_url.'/js_composer/assets/lib/nivoslider/themes/default/default.css' , array('nivo-slider-css'), null, 'screen' );
        wp_register_style( 'prettyphoto', $wp_plugin_url.'/js_composer/assets/lib/prettyphoto/css/prettyPhoto.css' , false, null, 'screen' );
        wp_register_style( 'isotope-css', $wp_plugin_url.'/js_composer/assets/css/isotope.css' , false, false, 'all' );
		    
		     
        wp_enqueue_script( 'wpb_composer_front_js', $wp_plugin_url.'/js_composer/assets/js/js_composer_front.js' , array('jquery'),null,true);
		wp_enqueue_script( 'jquery_ui_tabs_rotate', $wp_plugin_url.'/js_composer/assets/lib/jquery-ui-tabs-rotate/jquery-ui-tabs-rotate.js' , array( 'jquery', 'jquery-ui-tabs' ), null, true);
        wp_enqueue_script( 'tweet', $wp_plugin_url.'/js_composer/assets/lib/jquery.tweet/jquery.tweet.js' , array('jquery'),null,true);
        wp_enqueue_script( 'isotope', $wp_plugin_url.'/js_composer/assets/lib/isotope/jquery.isotope.min.js' , array('jquery'),null,true);
        wp_enqueue_script( 'jcarousellite', $wp_plugin_url.'/js_composer/assets/lib/jcarousellite/jcarousellite_1.0.1.min.js' , array('jquery'),null,true);

        wp_enqueue_script( 'nivo-slider', $wp_plugin_url.'/js_composer/assets/lib/nivoslider/jquery.nivo.slider.pack.js', array('jquery'),null,true);
        wp_enqueue_script( 'flexslider', $wp_plugin_url.'/js_composer/assets/lib/flexslider/jquery.flexslider-min.js' , array('jquery'),null,true);
        wp_enqueue_script( 'prettyphoto', $wp_plugin_url.'/js_composer/assets/lib/prettyphoto/js/jquery.prettyPhoto.js' , array('jquery'),null,true);
        wp_enqueue_script( 'waypoints', $wp_plugin_url.'/js_composer/assets/lib/jquery-waypoints/waypoints.min.js' , array('jquery'),null,true);
		


		// Enqueue the Theme JS
		// Basics
		wp_enqueue_script('themetastic_tweenmax', themetastic_JAVASCRIPT."/TweenMax.min.js", array('jquery'),false,true);
		wp_enqueue_script('themetastic_modernizr_script', themetastic_JAVASCRIPT."/jquery.modernizr.min.js", array('jquery'),false,true);
		wp_enqueue_script('themetastic_isotope_script', themetastic_JAVASCRIPT."/jquery.isotope.min.js", array('jquery'),false,true);
		wp_enqueue_script('themetastic_easing_script', themetastic_JAVASCRIPT."/jquery.easing.1.3.js", array('jquery'),false,true);
		wp_enqueue_script('themetastic_fitvid_script', themetastic_JAVASCRIPT."/jquery.fitvid.js", array('jquery'),false,true);
		wp_enqueue_script('themetastic_backstretch_script', themetastic_JAVASCRIPT."/jquery.backstretch.min.js", array('jquery'),false,false);
		wp_enqueue_script('themetastic_bootstrap_script', themetastic_JAVASCRIPT."/bootstrap.min.js", array('jquery'),false,true);
		wp_enqueue_script('themetastic_menu_script', themetastic_JAVASCRIPT."/ddsmoothmenu.js", array('jquery'),false,true);
		//wp_enqueue_script('themetastic_prettyphoto_script', themetastic_JAVASCRIPT."/jquery.prettyPhoto.js", array('jquery'),false,true);
		wp_enqueue_script('themetastic_fancybox_script', themetastic_JAVASCRIPT."/fancybox/jquery.fancybox.pack.js?v=2.1.3", array('jquery'),false,true);
		wp_enqueue_script('themetastic_fancybox_script_media', themetastic_JAVASCRIPT."/fancybox/helpers/jquery.fancybox-media.js?v=1.0.5", array('jquery'),false,true);
		wp_enqueue_script('themetastic_retina_script', themetastic_JAVASCRIPT."/retina.js", array('jquery'),false,true);
		/*if(is_singular() && get_option("thread_comments")) wp_enqueue_script("comment-reply");*/

		// Main Script
		wp_enqueue_script('themetastic_screen_script', themetastic_JAVASCRIPT."/screen.js", array('jquery'),false,true);
		
		//Comments
		if(is_singular() && get_option("thread_comments")) wp_enqueue_script("comment-reply");
		
		//IE8
		add_action( 'wp_head', create_function( '',
		   'echo \'<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><link rel="stylesheet" id="themetastic_ie8" href="'.get_template_directory_uri().'/css/ie8.css" type="text/css" media="all"><![endif]-->\';'
		) );
		
	}
	
}
add_action('wp_enqueue_scripts', 'loadJSCSS');



function load_custom_wp_admin_style(){
    wp_register_style( 'custom_wp_admin_css', themetastic_CSS.'/page_options.css', false, '1.0.0' );
    wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action('admin_enqueue_scripts', 'load_custom_wp_admin_style');
   
?>