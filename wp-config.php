<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bcl');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'K4qa7Au3y5PbYNGB');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_(N+w&[VaMy.bfXBg|]~ 3T4aUV(-tl+I?:_ DEgD$-i4V&7lmX9xi0-H/{]>Z}H');
define('SECURE_AUTH_KEY',  '6&e5b^)O-sOpowxC:8|~Z%T^.0K?gZM#8JIQ7B *[GCL[cO$dl#Vi7h0_i9>N%M|');
define('LOGGED_IN_KEY',    'S6W|hhi:A9.k97&j|<Asg|~puMf?74E_F[CM;cO&3`{#}Qj28fiEU_hYNR3*<A[b');
define('NONCE_KEY',        '+.W3~LgnzSf?|aS5MasqK }w.!04;B:8<Q>h2-FD/b!(HgNP0CA%FK{gVdfpp:iz');
define('AUTH_SALT',        'KO<h|=<[@JS+=HE+uRT4dT)xs/|#,gB,PBPSa/RZfYM!2b<Tk3EG 0r_ =]%=S5I');
define('SECURE_AUTH_SALT', '=D{ZB6d~Ih9|#c4`]cr! u |fZY_)f+cx3NrGF)s#4_2AJLhuLZJc#*E7H7h]ti9');
define('LOGGED_IN_SALT',   '_QWLc_)~0Xu$@Wed3i+-z#%!L#v|WtX46uXGqD-4Un1-WSsQs8|y-0@3U9y/+N^)');
define('NONCE_SALT',       'puC-C{a~o.|Gc^H_%G.K|4?$}4uQ,db$#Y|HuWilI,,D.0@#S37i F8U~=,|#RcK');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
